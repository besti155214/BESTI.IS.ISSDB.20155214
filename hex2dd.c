#include <stdio.h>
int ctoi(char ch)
{
if(ch >= 'a') return (10 + ch -'a');
return ch - '0';
}
void hex2dd(char *s)
{
int n, flag = 0;
char *p;
p = s + 2;
while(*p) {
n = ctoi(*p) * 16 + ctoi(*++p) * 1;
if(flag > 0) printf(".");
printf("%d", n);
p++; flag++;
}
printf("\n");
}
int main(int argc,char *argv[])
{
hex2dd(argv[1]);
return 0;
} 
